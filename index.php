<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
    <?php
        if(isset($_POST['diemHocKi1'])&&isset($_POST['diemHocKi2'])){
        $diemHocKi1=(float)$_POST['diemHocKi1'];
        $diemHocKi2=(float)$_POST['diemHocKi2'];
        $diemTB=($diemHocKi1+($diemHocKi2*2))/3;
        $ketQua="";
        $hocLuc="";
        function result($diemTB,&$ketQua,&$hocLuc){
                if($diemTB>=5){
                    $ketQua="Được lên lớp";
                    if($diemTB>=5 && $diemTB<6.5){
                        $hocLuc="Trung bình";
                    }elseif($diemTB>=6.5 && $diemTB<8){
                        $hocLuc="Khá";
                    }else $hocLuc="Giỏi";
                    
                }else{
                    $ketQua="Ở lại lớp";
                    $hocLuc="Yếu";
                }
            }
        
                result($diemTB,$ketQua,$hocLuc);
                  
        }
             
    ?>
    <form action="index.php" method="post" >
        <h1>KẾT QUẢ HỌC TẬP</h1>
        <div class="form-content">
            <div class="form-content--input">
                <label>Điểm HK1</label>
                <input type="text" name="diemHocKi1" value="<?php echo isset($diemHocKi1)?$diemHocKi1:"";?>" >
            </div>
            <div class="form-content--input">
                <label>Điểm HK2</label>
                <input type="text" name="diemHocKi2"  value="<?php echo isset($diemHocKi2)?$diemHocKi2:"";?>" >
            </div>
            <div class="form-content--input">
                <label>Điểm trung bình</label>
                <input type="text" value="<?php echo isset($diemTB)?$diemTB:"";?>" readonly=”true”>
            </div>
            <div class="form-content--input">
                <label>Kết quả</label>
                <input type="text" value="<?php echo isset($ketQua)?$ketQua:"";?>" readonly=”true”>
            </div>
            <div class="form-content--input">
                <label>Xếp loại học lực</label>
                <input type="text" value="<?php echo isset($hocLuc)?$hocLuc:"";?>" readonly=”true”>
            </div>
        </div>
        <input type="submit" name="submit" value="Xem kết quả">
    </form>
    
</body>
</html>